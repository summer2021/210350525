package org.apache.pulsar.testclient;

public class TestAvro {
    String name;
    String slogan;

    public TestAvro() {
        this("Grommash Hellscream", "Lok'tar ogar!");
    }

    public TestAvro(String name, String slogan) {
        this.name = name;
        this.slogan = slogan;
    }

    @Override
    public String toString() {
        return "TestAvro{" +
                "name='" + name + '\'' +
                ", slogan='" + slogan + '\'' +
                '}';
    }
}
